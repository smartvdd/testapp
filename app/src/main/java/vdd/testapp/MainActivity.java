package vdd.testapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import vdd.testapp.fragments.FlexibleFragment;
import vdd.testapp.fragments.StickyFragment;

public class MainActivity extends FragmentActivity implements StickyFragment.OnStickyItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            return;
        }
        setFragmentInContainer(new StickyFragment(), getIntent().getExtras());
    }

    private void setFragmentInContainer(Fragment fragment, Bundle bundle) {
        if (findViewById(R.id.fragment_container) != null) {
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment).commit();
        }
    }

    @Override
    public void onStickyClick(String title) {
        Bundle bundle = new Bundle();
        bundle.putString(FlexibleFragment.TITLE_TEXT, title);
        replaceFragmentInContainer(new FlexibleFragment(), bundle);
    }

    private void replaceFragmentInContainer(Fragment fragment, Bundle bundle) {
        if (findViewById(R.id.fragment_container) != null) {
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().addToBackStack(fragment.getTag())
                    .replace(R.id.fragment_container, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
