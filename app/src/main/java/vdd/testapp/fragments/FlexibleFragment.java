package vdd.testapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vdd.testapp.R;
import vdd.testapp.RecyclerAdapter;
import vdd.testapp.utils.RecyclerUtils;
import vdd.testapp.divider.DividerDecoration;

public class FlexibleFragment extends Fragment {

    public static final String TITLE_TEXT = "flexible_title_text";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_flexible, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.anim_toolbar);
        toolbar.setTitle(getArguments().getString(TITLE_TEXT));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        prepareRecyclerView();
    }

    public void prepareRecyclerView() {
        final RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycler_view);
        InformationAdapter adapter = new InformationAdapter();
        adapter.addAll(getInfoArray());
        DividerDecoration dividerDecoration = new DividerDecoration(getContext());
        RecyclerUtils.prepareRecyclerView(getContext(), recyclerView, adapter, dividerDecoration);
    }

    private List<String> getInfoArray() {
        List<String> infoArray = new ArrayList();
        String infoText = getString(R.string.some_text);
        for (int i = 0; i < 100; i++) {
            infoArray.add(infoText);
        }
        return infoArray;
    }

    private class InformationAdapter extends RecyclerAdapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item, parent, false);
            return new RecyclerView.ViewHolder(view) {};
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final TextView textView = (TextView) holder.itemView;
            textView.setText(getItem(position));
        }
    }
}
