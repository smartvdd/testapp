package vdd.testapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.List;

import vdd.testapp.R;
import vdd.testapp.RecyclerAdapter;
import vdd.testapp.divider.DividerDecoration;
import vdd.testapp.utils.RecyclerUtils;

public class StickyFragment extends android.support.v4.app.Fragment {

    OnStickyItemClickListener onStickyItemClickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onStickyItemClickListener = (OnStickyItemClickListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement " + OnStickyItemClickListener.class.getSimpleName());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sticky_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareRecyclerView();
    }

    private void prepareRecyclerView() {
        final RecyclerView stickyRecyclerView = (RecyclerView) getActivity().findViewById(R.id.sticky_recycler);
        final StickyHeadersAdapter adapter = new StickyHeadersAdapter();
        adapter.addAll(getContentArray());
        DividerDecoration dividerDecoration = new DividerDecoration(getContext());
        RecyclerUtils.prepareRecyclerView(getContext(), stickyRecyclerView, adapter, dividerDecoration);
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        stickyRecyclerView.addItemDecoration(headersDecor);
    }

    private List<String> getContentArray() {
        List<String> someContent = new ArrayList<>();
        String content = getString(R.string.some_content);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                someContent.add(String.format("%s %s", content, i + 1));
            }
        }
        return someContent;
    }

    private class StickyHeadersAdapter extends RecyclerAdapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item, parent, false);
            return new RecyclerView.ViewHolder(view) {};
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final TextView textView = (TextView) holder.itemView;
            textView.setText(getItem(position));
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStickyItemClickListener.onStickyClick(textView.getText().toString());
                }
            });
        }

        @Override
        public long getHeaderId(int position) {
            return getItem(position).charAt(getItem(position).length() - 1);
        }

        @Override
        public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_header, parent, false);
            return new RecyclerView.ViewHolder(view) {};
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
            TextView textView = (TextView) holder.itemView;
            textView.setText(String.format("%s %s", getString(R.string.title), getItem(position).replaceAll("[^\\d]", "")));
        }
    }

    public interface OnStickyItemClickListener {

        void onStickyClick(String title);

    }
}
