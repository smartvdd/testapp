package vdd.testapp.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class RecyclerUtils {

    public static void prepareRecyclerView(Context context, RecyclerView recyclerView, RecyclerView.Adapter<RecyclerView.ViewHolder> adapter, RecyclerView.ItemDecoration itemDecoration) {
        if (recyclerView != null && context != null) {
            recyclerView.setAdapter(adapter);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.addItemDecoration(itemDecoration);
        }
    }
}
